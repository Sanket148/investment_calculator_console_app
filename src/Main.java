import java.util.Scanner;

public class Main {
public static void main(String args[]) throws Exception {
	
	Scanner sc=new Scanner(System.in);
	
	
	Lumpsum c2=new Lumpsum();
	Sip c1 =new Sip();
	int option=0;
	
	while (option!=3) {
		showOption();
		System.out.println("\n Enter Your option : ");
		option=sc.nextInt();
		
		if(option==1) {
			c1.sip();
			}
		else if(option==2) {
			c2.lumpsum();
		}
		else if(option==3) {
			System.out.println("EXIT!!!!!");
			exit();
		}
		else {
			System.out.println("Re-Enter The Option!!!!!");
		}
	}
}

public static void showOption(){
	System.out.println("Choose Your Investment Plan");
	System.out.println("\t1.SIP"
			+ "      *(It is monthly investment plan)");
	System.out.println("\t2.LUMPSUM"
			+ "      *(It is one time investment plan)");
	System.out.println("\t3.EXIT");
}
public static void exit() throws Exception
{
	
	System.exit(0);
}


}
